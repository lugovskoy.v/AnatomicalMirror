using UnityEngine;
using System;

public class SimpleGestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
    public delegate void GestureDetectedCallback(KinectGestures.Gestures gesture);

    // private bool to track if progress message has been displayed
    private bool progressDisplayed;
	private float progressGestureTime;
    private GestureDetectedCallback gestureDetected = null;

    public void SetGestureDetectCallback(GestureDetectedCallback callback) {
        gestureDetected = callback;
    }

    public void UserDetected(long userId, int userIndex)
	{
		// as an example - detect these user specific gestures
		KinectManager manager = KinectManager.Instance;
		manager.DetectGesture(userId, KinectGestures.Gestures.Jump);
		manager.DetectGesture(userId, KinectGestures.Gestures.Squat);
		manager.DetectGesture(userId, KinectGestures.Gestures.LeanLeft);
		manager.DetectGesture(userId, KinectGestures.Gestures.LeanRight);

	}
	
	public void UserLost(long userId, int userIndex)
	{
	}

	public void GestureInProgress(long userId, int userIndex, KinectGestures.Gestures gesture, 
	                              float progress, KinectInterop.JointType joint, Vector3 screenPos)
	{
		if((gesture == KinectGestures.Gestures.ZoomOut || gesture == KinectGestures.Gestures.ZoomIn) && progress > 0.5f)
		{
			string sGestureText = string.Format ("{0} - {1:F0}%", gesture, screenPos.z * 100f);
            //Debug.Log(sGestureText);
			progressDisplayed = true;
			progressGestureTime = Time.realtimeSinceStartup;
		}
		else if((gesture == KinectGestures.Gestures.Wheel || gesture == KinectGestures.Gestures.LeanLeft || 
		         gesture == KinectGestures.Gestures.LeanRight) && progress > 0.5f)
		{
			string sGestureText = string.Format ("{0} - {1:F0} degrees", gesture, screenPos.z);
			//Debug.Log(sGestureText);
			progressDisplayed = true;
			progressGestureTime = Time.realtimeSinceStartup;
		}
	}

	public bool GestureCompleted(long userId, int userIndex, KinectGestures.Gestures gesture, 
	                              KinectInterop.JointType joint, Vector3 screenPos)
	{
		if(progressDisplayed)
			return true;

		string sGestureText = gesture + " detected";
        //Debug.Log(sGestureText);
		
        if(gestureDetected != null) {
            gestureDetected(gesture);
        }

		return true;
	}

	public bool GestureCancelled(long userId, int userIndex, KinectGestures.Gestures gesture, 
	                              KinectInterop.JointType joint)
	{
		if(progressDisplayed)
		{
			progressDisplayed = false;
		}
		return true;
	}
}
