﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TextLayerDescription {
    public float zMin, zMax;
    public string layerName;
    //public TextLabel[] maleLabels;
    //public TextLabel[] femaleLabels;
}

[System.Serializable]
public class FocusController : MonoBehaviour {
    public GameObject models;
    public Text textZ;
    public Text textLayerTitle;
    public TextLayerDescription[] layers;
    public int curLayerIdx = -1;
    public Gender curGender;
    public GenderSwitcher gs;

    public LanguageEnum language = LanguageEnum.Russian;
    public TextLayerController[] cTextLayers;

    int CalculateLayerIdx() {
        for(int i = 0; i < layers.Length; ++i) {
            if(AvatarController.zValue >= layers[i].zMin && AvatarController.zValue < layers[i].zMax) {
                return i;
            }
        }
        return -1;
    }

    void HideTextLayers() {
        for(int i = 0; i < layers.Length; ++i) {
            HideTextLayer(i);
        }
        curLayerIdx = -1;
    }

    void HideTextLayer(int idx) {
        if (idx < 0 || idx >= layers.Length) {
            return;
        }

        for(int i = 0; i < cTextLayers.Length; ++i) {
            cTextLayers[i].Hide();
        }
        //for (int i = 0; i < layers[idx].maleLabels.Length; ++i) {
        //    layers[idx].maleLabels[i].Hide();
        //}
        //for (int i = 0; i < layers[idx].femaleLabels.Length; ++i) {
        //    layers[idx].femaleLabels[i].Hide();
        //}
        textLayerTitle.text = string.Empty;
    }

    void ShowTextLayer(int idx) {
        if (idx < 0 || idx >= layers.Length) {
            return;
        }
        HideTextLayers();

        for (int i = 0; i < cTextLayers.Length; ++i) {
            if(cTextLayers[i].language == language) {
                cTextLayers[i].ShowLayer(idx, gs.curGender);
                textLayerTitle.text = cTextLayers[i].layerNames[idx];
                break;
            }
        }
        //if (gs.curGender == Gender.Male) {
        //    for (int i = 0; i < layers[idx].maleLabels.Length; ++i) {
        //        layers[idx].maleLabels[i].Show();
        //    }
        //} else {
        //    for (int i = 0; i < layers[idx].femaleLabels.Length; ++i) {
        //        layers[idx].femaleLabels[i].Show();
        //    }
        //}
        curLayerIdx = idx;
        //textLayerTitle.text = layers[idx].layerName;
    }

	void Update () {
        if(Input.GetKeyDown(KeyCode.F1)) {
            language = LanguageEnum.Russian;
            ShowTextLayer(curLayerIdx);
        }
        if (Input.GetKeyDown(KeyCode.F2)) {
            language = LanguageEnum.Ukrainian;
            ShowTextLayer(curLayerIdx);
        }
        if (Input.GetKeyDown(KeyCode.F3)) {
            language = LanguageEnum.English;
            ShowTextLayer(curLayerIdx);
        }
        if (KinectManager.Instance.GetUsersCount() == 0) {
            textZ.text = "Z: none";
            models.SetActive(false);
            HideTextLayers();
        } else {
            if(!models.activeSelf) {
                models.SetActive(true);
            }
            textZ.text = string.Format("Z: {0}", AvatarController.zValue);
            int layerIdx = CalculateLayerIdx();
            if (layerIdx != curLayerIdx || curGender != gs.curGender) {
                curGender = gs.curGender;
                ShowTextLayer(layerIdx);
            }
        }
	}
}
