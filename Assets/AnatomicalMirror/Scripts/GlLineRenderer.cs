﻿using UnityEngine;

[ExecuteInEditMode]
public class GlLineRenderer : MonoBehaviour {
    public Vector3[] points = new Vector3[3];
    public Color color = Color.black;
    public bool isRendering = true;
    static Material lineMaterial;
    static void CreateLineMaterial(Color color) {
        if (!lineMaterial) {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            lineMaterial.SetInt("_ZWrite", 0);
            lineMaterial.color = color;
        }
    }

    public void OnRenderObject() {
        if(!isRendering) {
            return;
        }
        CreateLineMaterial(color);
        lineMaterial.SetPass(0);
        GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        transform.position = Vector3.zero;
        GL.MultMatrix(transform.localToWorldMatrix);
        GL.Color(color);
        GL.Begin(GL.LINES);
        for (int i = 1; i < points.Length; ++i) {
            GL.Vertex3(points[i-1].x, points[i - 1].y, points[i - 1].z);
            GL.Vertex3(points[i].x, points[i].y, points[i].z);
        }
        GL.End();
        GL.PopMatrix();
    }
}
