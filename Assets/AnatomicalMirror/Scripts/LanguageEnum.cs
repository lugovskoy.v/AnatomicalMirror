﻿[System.Serializable]
public enum LanguageEnum {
    Russian = 0,
    English = 1,
    Ukrainian = 2
}
