﻿using UnityEngine;

public enum Gender {
    Female,
    Male
}

public class GenderSwitcher : MonoBehaviour {
    public Gender curGender = Gender.Male;
    public GameObject maleParent;
    public GameObject femaleParent;
    ///// <summary>
    ///// The hand bone which will be used for activation of gender switch
    ///// </summary>
    //public Transform activeHand;
    //public Rect screenRect;
    //public Camera mainCamera;
    //public bool genderSwitched = false;
    public GenderIconAnimator iconAnim;
    public const float DEFAULT_SWITCH_COOLDOWN = 1f;
    private float switchCooldown = 0f;

	void Awake () {
        Reset();
	}

    void Update() {
        if (switchCooldown > 0f) {
            switchCooldown -= Time.deltaTime;
        }
    }

    void OnTriggerEnter(Collider c) {
        if(switchCooldown > 0f) {
            return;
        }
        Switch();
        Debug.Log("Gender switched!");
    }

    //void OnDrawGizmos() {
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireCube(screenRect.center, screenRect.size);
    //}

    //void Update() {
        //Debug.Log(screenRect  + " - " + mainCamera.WorldToViewportPoint(activeHand.position));
        //if (screenRect.Contains(mainCamera.WorldToViewportPoint(activeHand.position))) {
        //    if(!genderSwitched) {
        //        Switch();
        //        genderSwitched = true;
        //    }
        //} else if(genderSwitched) {
        //    genderSwitched = false;
        //}
    //}

    public void Reset() {
        maleParent.SetActive(false);
        femaleParent.SetActive(false);
        ActivateGender(curGender);
    }

    public void Switch() {
        ActivateGender(curGender == Gender.Male ? Gender.Female : Gender.Male);
        switchCooldown = DEFAULT_SWITCH_COOLDOWN;
    }

    public void ActivateGender(Gender gender) {
        if(gender == Gender.Male) {
            femaleParent.SetActive(false);
            maleParent.SetActive(true);
        } else {
            maleParent.SetActive(false);
            femaleParent.SetActive(true);
        }
        this.curGender = gender;
        iconAnim.SetGender(curGender);
    }
}
