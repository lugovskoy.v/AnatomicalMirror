﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

[System.Serializable]
[ExecuteInEditMode]
public class TextLabel : MonoBehaviour {
    public Text text;
    private RectTransform rtText;
    public GlLineRenderer rLine;
    public Transform target;

    public const float FixedZ = -5f;

	void Start () {
        rtText = text.GetComponent<RectTransform>();
        rLine.points = new Vector3[2];
	}
	
	void Update () {
        var startPos = text.transform.position;
        startPos.z = FixedZ;
        var endPos = target.position;
        endPos.z = FixedZ;
        //startPos.y -= rtText.sizeDelta.y / 600f;
        startPos.x += rtText.sizeDelta.x / (rtText.position.x < target.position.x ? 600f : -600f);
        //startPos.x += rtText.sizeDelta.x / (rtText.position.x < target.position.x ? 300f : -300f);
        rLine.points[0] = startPos;
        //rLine.points[1] = startPos;
        rLine.points[1] = endPos;
	}

    public void Show() {
        rLine.gameObject.SetActive(true);
        text.gameObject.SetActive(true);
    }

    public void Hide() {
        rLine.gameObject.SetActive(false);
        text.gameObject.SetActive(false);
    }
}
