﻿using UnityEngine;
using UnityEngine.UI;

public class GenderIconAnimator : MonoBehaviour {
    public RectTransform imgMale;
    public RectTransform imgFemale;
    public Gender gender = Gender.Male;
    public Vector3 minScale;
    public Vector3 maxScale;
    public float smoothTime = 0.5f;
    private Vector3 maleVelocity, femaleVelocity;
    private Vector3 maleVelocity2, femaleVelocity2;
    public Vector3 minFemalePos, minMalePos;

    void Update () {
        imgMale.localScale = Vector3.SmoothDamp(imgMale.localScale, gender == Gender.Male ? maxScale : minScale, ref maleVelocity, smoothTime);
        imgMale.localPosition = Vector3.SmoothDamp(imgMale.localPosition, gender == Gender.Male ? Vector3.zero : minMalePos, ref maleVelocity2, smoothTime);
        imgFemale.localScale = Vector3.SmoothDamp(imgFemale.localScale, gender == Gender.Female ? maxScale : minScale, ref femaleVelocity, smoothTime);
        imgFemale.localPosition = Vector3.SmoothDamp(imgFemale.localPosition, gender == Gender.Female ? Vector3.zero : minFemalePos, ref femaleVelocity2, smoothTime);
    } 

    public void SetGender(Gender gender) {
        this.gender = gender;
    }
}
