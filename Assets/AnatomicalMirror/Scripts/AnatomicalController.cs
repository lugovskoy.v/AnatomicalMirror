﻿namespace AnatomicalMirror {
    using UnityEngine;
    using System.Collections.Generic;
    using System.Collections;
    using UnityEngine.UI;

    [System.Serializable]
    public class ModelDescription {
        public GameObject goModel;
        public List<Material> materials;
        public Transform motionRoot;
        public List<AlphaOverride> alphaOverrides;
    }

    [System.Serializable]
    public class LayerDescription {
        public List<int> modelIndexes;
        public float zValue;
    }

    public enum BlendMode {
        Opaque,
        Cutout,
        Fade,
        Transparent
    }

    [System.Serializable]
    public struct AlphaOverride {
        [Range(0.0f, 1.0f)]
        public float maxAlpha;
        public int materialId;
    }

    [System.Serializable]
    public enum Gender {
        Female,
        Male
    }

    public class AnatomicalController : MonoBehaviour {
        public SimpleGestureListener gestureListener = null;
        public ModelDescription[] modelsMale;
        public ModelDescription[] modelsFemale;
        public LayerDescription[] layersMale;
        public LayerDescription[] layersFemale;

        //public int currentModelIdx;
        public int currentLayerIdx = 0;
        private bool layerTransitionInProgress = false;
        protected int? lastModelidx = null;
        public Text textTitle;
        public Text[] textLabels;
        public Gender curGender = Gender.Male;

        void Awake() {
            for (int k = 0; k < modelsMale.Length; ++k) {
                for (int i = 0; i < modelsMale[k].materials.Count; ++i) {
                    var mat = modelsMale[k].materials[i];
                    SetShaderBlendMode(BlendMode.Fade, ref mat);
                    Color c = mat.color;
                    c.a = 0f;
                    mat.SetColor("_Color", c);
                    modelsMale[k].materials[i] = mat;
                }
            }
            for (int k = 0; k < modelsFemale.Length; ++k) {
                for (int i = 0; i < modelsFemale[k].materials.Count; ++i) {
                    var mat = modelsFemale[k].materials[i];
                    SetShaderBlendMode(BlendMode.Fade, ref mat);
                    Color c = mat.color;
                    c.a = 0f;
                    mat.SetColor("_Color", c);
                    modelsFemale[k].materials[i] = mat;
                }
            }
            currentLayerIdx = -1;
        }

        void Start() {
            if(gestureListener == null) {
                Debug.LogError("Gesture listener isn't initialized!");
                this.enabled = false;
                return;
            }
            var models = curGender == Gender.Male ? modelsMale : modelsFemale;
            var layers = curGender == Gender.Male ? layersMale : layersFemale;
            if (models.Length == 0) {
                Debug.LogError("No models to control");
                return;
            }
            if(currentLayerIdx < 0 || currentLayerIdx >= layers.Length) {
                Debug.LogWarning("Start layer index is out of range. First layer used by default");
                currentLayerIdx = 0;
            }
            gestureListener.SetGestureDetectCallback(GestureDetected);
        }

        public void ChangeGender() {
            FadeLayerModels();
            curGender = curGender == Gender.Male ? Gender.Female : Gender.Male;
            var layers = curGender == Gender.Male ? layersMale : layersFemale;
            currentLayerIdx = -1;
        }

        private float userNonTrackingTime = 0f;
        private const float UserNonTrackingFadeTime = 0.2f;

        void Update() {
            if(Input.GetKeyDown(KeyCode.Space)) {
                ChangeGender();
            }

            //models[0].materials[0].SetVector("_HipsPos", models[0].goModel.GetComponent<AvatarControllerClassic>().BodyRoot.position);
            if (AvatarController.IsUserTracked()) {
                userNonTrackingTime = 0f;
                float curPos = AvatarController.zValue;
                var layers = curGender == Gender.Male ? layersMale : layersFemale;
                if (curPos > layers[0].zValue && currentLayerIdx != 0) {
                    ShowLayer(0);
                } else if (curPos > layers[layers.Length - 1].zValue) {
                    for (int i = 1; i < layers.Length; ++i) {
                        if (curPos > layers[1].zValue && curPos <= layers[0].zValue && currentLayerIdx != i) {
                            ShowLayer(i);
                        }
                    }
                } else {
                    StartCoroutine(FadeLayerModels());
                    currentLayerIdx = -1;
                }
            } else {
                userNonTrackingTime += Time.deltaTime;
                if (userNonTrackingTime > UserNonTrackingFadeTime) {
                    StartCoroutine(FadeLayerModels());
                    currentLayerIdx = -1;
                }
            }
        }
        protected void ShowLayer(int idx) {
            var layers = curGender == Gender.Male ? layersMale : layersFemale;
            if (idx < 0 || idx >= layers.Length) {
                return;
            }
            Debug.Log("Showing layer: " + idx);
            if(layerTransitionInProgress) {
                Debug.LogWarning("ShowLayer canceled: layer transition in progress");
                return;
            }
            layerTransitionInProgress = true;
            StartCoroutine(FadeLayerModels());
            currentLayerIdx = idx;
            StartCoroutine(ShowLayerModels(layers[currentLayerIdx]));
        }

        public static void SetShaderBlendMode(BlendMode blendMode, ref Material material) {
            switch (blendMode) {
                case BlendMode.Opaque:
                    material.SetFloat("_Mode", (float)BlendMode.Opaque);
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt("_ZWrite", 1);
                    material.DisableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = -1;
                    break;
                case BlendMode.Cutout:
                    material.SetFloat("_Mode", (float)BlendMode.Cutout);
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt("_ZWrite", 1);
                    material.EnableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 2450;
                    break;
                case BlendMode.Fade:
                    material.SetFloat("_Mode", (float)BlendMode.Fade);
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    material.SetInt("_ZWrite", 0);
                    material.DisableKeyword("_ALPHATEST_ON");
                    material.EnableKeyword("_ALPHABLEND_ON");
                    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 3000;
                    break;
                case BlendMode.Transparent:
                    material.SetFloat("_Mode", (float)BlendMode.Transparent);
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    material.SetInt("_ZWrite", 0);
                    material.DisableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 3000;
                    break;
            }

        }

        public IEnumerator FadeLayerModels(/*ModelDescription model*/) {
            if (currentLayerIdx != -1) {
                //Debug.Log("FADING!");   
                var models = curGender == Gender.Male ? modelsMale : modelsFemale;
                for (int k = 0; k < models.Length; ++k) {
                    for (int i = 0; i < models[k].materials.Count; ++i) {
                        var mat = models[k].materials[i];
                        SetShaderBlendMode(BlendMode.Fade, ref mat);
                        models[k].materials[i] = mat;
                    }
                }
                for (float f = 1f; f > 0f; f -= 0.2f) {
                    for (int k = 0; k < models.Length; ++k) {
                        for (int j = 0; j < models[k].materials.Count; ++j) {
                            Color c = models[k].materials[j].color;
                            if(c.a < f) {
                                continue;
                            }
                            c.a = f;
                            models[k].materials[j].SetColor("_Color", c);
                        }
                    }
                    yield return new WaitForSeconds(.1f);
                }
                for (int k = 0; k < models.Length; ++k) {
                    for (int j = 0; j < models[k].materials.Count; ++j) {
                        Color c = models[k].materials[j].color;
                        c.a = 0f;
                        models[k].materials[j].SetColor("_Color", c);
                    }
                }
            }
        }

        private float BoundMaxAlpha(ModelDescription model, int materialId, float alpha, bool isFinalPass = false) {
            float maxAlpha = 1f;
            for (int i = 0; i < model.alphaOverrides.Count; ++i) {
                if (materialId == model.alphaOverrides[i].materialId) {
                    maxAlpha = model.alphaOverrides[i].maxAlpha;
                    break;
                }
            }
            if (alpha > maxAlpha) {
                alpha = maxAlpha;
            }
            return isFinalPass ? maxAlpha : alpha;
        }

        public IEnumerator ShowLayerModels(LayerDescription layer) {
            var models = curGender == Gender.Male ? modelsMale : modelsFemale;
            yield return new WaitForSeconds(.55f);
            for (int k = 0; k < layer.modelIndexes.Count; ++k) {
                for (int i = 0; i < models[layer.modelIndexes[k]].materials.Count; ++i) {
                    var mat = models[layer.modelIndexes[k]].materials[i];
                    SetShaderBlendMode(BlendMode.Fade, ref mat);
                    models[layer.modelIndexes[k]].materials[i] = mat;
                    Color c = models[layer.modelIndexes[k]].materials[i].color;
                    c.a = 0f;
                    models[layer.modelIndexes[k]].materials[i].SetColor("_Color", c);
                }
            }
            for (float f = 0f; f < 1f; f += 0.2f) {
                for (int k = 0; k < layer.modelIndexes.Count; ++k) {
                    for (int j = 0; j < models[layer.modelIndexes[k]].materials.Count; ++j) {
                        Color c = models[layer.modelIndexes[k]].materials[j].color;
                        c.a = BoundMaxAlpha(models[k], j, f);
                        models[layer.modelIndexes[k]].materials[j].SetColor("_Color", c);
                    }
                }
                yield return new WaitForSeconds(.1f);
            }
            for (int k = 0; k < layer.modelIndexes.Count; ++k) {
                for (int i = 0; i < models[layer.modelIndexes[k]].materials.Count; ++i) {
                    Color c = models[layer.modelIndexes[k]].materials[i].color;
                    c.a = BoundMaxAlpha(models[k], i, c.a, true);
                    var blendMode = c.a < 1.0f ? BlendMode.Fade : BlendMode.Opaque;
                    models[layer.modelIndexes[k]].materials[i].SetColor("_Color", c);
                    var mat = models[layer.modelIndexes[k]].materials[i];
                    SetShaderBlendMode(blendMode, ref mat);
                    models[layer.modelIndexes[k]].materials[i] = mat;
                }
            }
            layerTransitionInProgress = false;
        }

        protected void SquatDetected() {
            //Debug.Log("Squat detected!");
            //NextModel();
        }

        protected void SwipeRightDetected() {
            //ShowLayer();
        }

        protected void SwipeLeftDetected() {
            //ShowLayer(false);
        }

        protected void SwipeUpDetected() {
            //NextModel();
        }

        protected void SwipeDownDetected() {
            //NextModel();
        }


        public void GestureDetected(KinectGestures.Gestures gesture) {
            switch(gesture) {
                case KinectGestures.Gestures.Squat:
                    SquatDetected();
                    break;
                case KinectGestures.Gestures.SwipeLeft:
                    SwipeLeftDetected();
                    break;
                case KinectGestures.Gestures.SwipeRight:
                    SwipeRightDetected();
                    break;
                case KinectGestures.Gestures.SwipeUp:
                    SwipeUpDetected();
                    break;
                case KinectGestures.Gestures.SwipeDown:
                    SwipeDownDetected();
                    break;
                case KinectGestures.Gestures.RaiseLeftHand:
                    Debug.Log("HAND RISED LEFT");
                    break;
                case KinectGestures.Gestures.RaiseRightHand:
                    Debug.Log("HAND RISED RIGHT");
                    break;
                default:
                    Debug.Log("Gesture handler is missing: " + gesture);
                    break;
            }
        }
    }
}
