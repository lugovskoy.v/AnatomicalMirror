﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI.Extensions;

[RequireComponent(typeof(UILineRenderer))]
public class BezierLineRenderer : MonoBehaviour {
    private UILineRenderer lineRenderer;
    public RectTransform from;
    public RectTransform to;

    void Start () {
        lineRenderer = GetComponent<UILineRenderer>();
        lineRenderer.BezierMode = UILineRenderer.BezierType.Quick;
        lineRenderer.Points = new Vector2[2];
    }
	
	void Update () {
        lineRenderer.Points[0] = from.localPosition;//from.position * 100f;
        lineRenderer.Points[1] = to.position * 100f  - from.position * 100f + from.localPosition;
        lineRenderer.SetAllDirty();
    }
}
