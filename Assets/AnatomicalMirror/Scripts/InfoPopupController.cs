﻿using UnityEngine;
using System.Collections;

public class InfoPopupController : MonoBehaviour {

    public CanvasGroup cGroup;
    public float alpha = 0.0f;
    public float zBorder = 2f;

	// Update is called once per frame
	void Update () {
        if (cGroup.alpha == 0f && transform.position.z <= zBorder) {
            cGroup.alpha = 1f;
        }
        if (cGroup.alpha == 1f && transform.position.z > zBorder) {
            cGroup.alpha = 0f;
        }
    }
}
