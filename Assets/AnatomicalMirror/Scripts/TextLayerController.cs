﻿using System.Collections.Generic;
using UnityEngine;

public class TextLayerController : MonoBehaviour {
    public LanguageEnum language;
    public GameObject[] layersMale;
    public GameObject[] layersFemale;
    public string[] layerNames;
    private List<TextLabel>[] labelsMale;
    private List<TextLabel>[] labelsFemale;

    void Start () {
        labelsMale = new List<TextLabel>[layersMale.Length];
	    for(int i = 0; i < layersMale.Length; ++i) {
            var labels = layersMale[i].GetComponentsInChildren<TextLabel>();
            labelsMale[i] = new List<TextLabel>(labels.Length);
            for(int j = 0; j < labels.Length; ++j) {
                labelsMale[i].Add(labels[j]);
            }
        }
        labelsFemale = new List<TextLabel>[layersFemale.Length];
        for (int i = 0; i < layersFemale.Length; ++i) {
            var labels = layersFemale[i].GetComponentsInChildren<TextLabel>();
            labelsFemale[i] = new List<TextLabel>(labels.Length);
            for (int j = 0; j < labels.Length; ++j) {
                labelsFemale[i].Add(labels[j]);
            }
        }
    }
	
    public void Hide() {
        for(int i = 0; i < labelsMale.Length; ++i) {
            for(int j = 0; j < labelsMale[i].Count; ++j) {
                labelsMale[i][j].Hide();
            }
        }
        for (int i = 0; i < labelsFemale.Length; ++i) {
            for (int j = 0; j < labelsFemale[i].Count; ++j) {
                labelsFemale[i][j].Hide();
            }
        }
    }

    public void ShowLayer(int idx, Gender gender) {
        Hide();
        var labels = gender == Gender.Male ? labelsMale : labelsFemale;
        for(int i = 0; i < labels[idx].Count; ++i) {
            labels[idx][i].Show();
        }
    }
}
